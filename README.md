# Getting Started
* Install Terraform
* Clone this repo
* Run `terraform init` from this directory
* See the plan with `terraform plan`
