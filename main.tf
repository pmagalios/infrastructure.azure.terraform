#hardcoded values for the the terraform run, these can be changed no big deal
locals {
  iprange     = "10.0.0.0/16"
  username    = "terraform_usr_1"
  password    = "%$GYR&sb7CFN*oMLAtqE"
  sa_username = "terraform_sa"
}

# Configure the Azure provider
terraform {
  required_providers {
    azurerm   = {
     version  = ">= 2.46"
    }
  }
}

provider "azurerm" {
  features {}
  subscription_id = "7d1968fb-7706-4c31-868d-e837c46f3ffa"
}

#Set up the resource group
resource "azurerm_resource_group" "rg" {
  name     = "${var.prefix}-rg"
  location = var.region
}

#Set up the network security group
resource "azurerm_network_security_group" "nsg" {
  name                = "${var.prefix}-nsg"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  }

resource "azurerm_virtual_network" "vnet" {
  name                = "${var.prefix}-vnet"
  location            = azurerm_network_security_group.nsg.location
  resource_group_name = azurerm_resource_group.rg.name
  address_space       = [ local.iprange ]
  depends_on          = [
    azurerm_resource_group.rg
  ]
}

resource "azurerm_subnet" "vnet_sub_1" {
  name = "${var.prefix}-subnet-1"
  address_prefixes     = [ "10.0.0.0/24" ]
  virtual_network_name = azurerm_virtual_network.vnet.name
  resource_group_name  = azurerm_resource_group.rg.name
  depends_on           = [
    azurerm_virtual_network.vnet
  ]
}

resource "azurerm_subnet" "vnet_sub_2" {
  name = "${var.prefix}-subnet-2"
  address_prefixes      = [ "10.0.1.0/24" ]
  virtual_network_name  = azurerm_virtual_network.vnet.name
  resource_group_name   = azurerm_resource_group.rg.name
  depends_on            = [
    azurerm_virtual_network.vnet
  ]
}

resource "azurerm_subnet_network_security_group_association" "nsg_sub1" {
  subnet_id                 = azurerm_subnet.vnet_sub_1.id
  network_security_group_id = azurerm_network_security_group.nsg.id
  depends_on                = [
    azurerm_subnet.vnet_sub_1,
    azurerm_network_security_group.nsg
  ]
}

resource "azurerm_subnet_network_security_group_association" "nsg_sub2" {
  subnet_id                 = azurerm_subnet.vnet_sub_2.id
  network_security_group_id = azurerm_network_security_group.nsg.id
  depends_on                = [
    azurerm_subnet.vnet_sub_2,
    azurerm_network_security_group.nsg
  ]
}

resource "azurerm_storage_account" "sa" {
  name = "ih9dgidysvjgjb9ew448"
  resource_group_name       = azurerm_resource_group.rg.name
  location                  = azurerm_resource_group.rg.location
  account_tier              = "Standard"
  account_replication_type  = "LRS"
  allow_blob_public_access  = false

   network_rules {
     default_action         = "Allow"
     bypass                 = [ "AzureServices" ]
   }
}

resource "azurerm_storage_share" "fs" {
  name                  = "${var.prefix}-fileshare"
  storage_account_name  = azurerm_storage_account.sa.name
  quota                 = 500
  depends_on            = [
    azurerm_storage_account.sa
  ]
}

resource "azurerm_mssql_server" "sql_server" {
  name                          = "${var.prefix}-sql-server"
  resource_group_name           = azurerm_resource_group.rg.name
  location                      = azurerm_resource_group.rg.location
  version                       = "12.0"
  administrator_login           = local.sa_username
  administrator_login_password  = local.password
}

resource "azurerm_mssql_database" "sql_database" {
  name                        = "${var.prefix}-sql-database"
  server_id                   = azurerm_mssql_server.sql_server.id
  depends_on                  = [
    azurerm_mssql_server.sql_server
  ]
  # license_type                = "LicenseIncluded"
  auto_pause_delay_in_minutes = -1
  create_mode                 = "Default"
  geo_backup_enabled          = true
  max_size_gb                 = 500
  min_capacity                = 2
  sku_name                    = "GP_S_Gen5_8"
}

resource "azurerm_public_ip" "lb_ip" {
  name                = "${var.prefix}-lb-public-ip"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_lb" "lb" {
  resource_group_name     = azurerm_resource_group.rg.name
  location                = azurerm_resource_group.rg.location
  name                    = "${var.prefix}-lb"
  sku                     = "Standard"
  depends_on              = [
    azurerm_public_ip.lb_ip
  ]
  
  frontend_ip_configuration {
    name                  = "${var.prefix}-lb-frontend-config"
    public_ip_address_id  = azurerm_public_ip.lb_ip.id
  }
}

resource "azurerm_lb_backend_address_pool" "lb_backend" {
  name            = "${var.prefix}-lb-backend-address-pool"
  loadbalancer_id = azurerm_lb.lb.id
  depends_on      = [
    azurerm_lb.lb
  ]
}

resource "azurerm_network_interface_backend_address_pool_association" "lb_backend_int" {
  network_interface_id    = azurerm_network_interface.ni1.id
  ip_configuration_name   = azurerm_network_interface.ni1.ip_configuration[0].name
  backend_address_pool_id = azurerm_lb_backend_address_pool.lb_backend.id
  depends_on              = [
    azurerm_network_interface.ni1,
    azurerm_lb_backend_address_pool.lb_backend

  ]
}

resource "azurerm_public_ip" "vm1_ip" {
  name                = "${var.prefix}-vm1-public-ip"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_network_interface" "ni1" {
  name                          = "${var.prefix}-vm1-interface-1"
  resource_group_name           = azurerm_resource_group.rg.name
  location                      = azurerm_resource_group.rg.location
  depends_on                    = [
    azurerm_subnet.vnet_sub_2,
    azurerm_public_ip.vm1_ip,
    azurerm_lb_backend_address_pool.lb_backend
  ]
  
  ip_configuration {
    name                          = "${var.prefix}-vm1-interface-1-ip"
    subnet_id                     = azurerm_subnet.vnet_sub_2.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.vm1_ip.id
  }
}

resource "azurerm_network_interface_security_group_association" "ni1_nsg" {
  network_interface_id      = azurerm_network_interface.ni1.id
  network_security_group_id = azurerm_network_security_group.nsg.id
  depends_on                = [
    azurerm_network_interface.ni1,
    azurerm_network_security_group.nsg
  ]
}

resource "azurerm_virtual_machine" "vm1" {
  name                              = "${var.prefix}-vm1"
  resource_group_name               = azurerm_resource_group.rg.name
  location                          = azurerm_resource_group.rg.location
  network_interface_ids             = [azurerm_network_interface.ni1.id]
  vm_size                           = "Standard_D2s_v3"
  license_type                      = "Windows_Server"
  delete_os_disk_on_termination     = true
  delete_data_disks_on_termination  = true
  depends_on                        = [
    azurerm_network_interface.ni1
  ]

  tags = {
    "app" = "octopus"
  }

  storage_os_disk {
    name                            = "${var.prefix}-vm1-disk-os"
    caching                         = "ReadWrite"
    create_option                   = "FromImage"
    managed_disk_type               = "Standard_LRS"
  }

  #Set host specific Details
  os_profile {
    computer_name                   = "${var.prefix}-vm1"
    admin_username                  = local.username
    admin_password                  = local.password
  }
  
  #Set details about the image that is going on the OS Disk
  storage_image_reference {
    publisher                       = "MicrosoftWindowsServer"
    offer                           = "WindowsServer"
    sku                             = "2019-Datacenter"
    version                         = "latest"
  }

  #Tell azure that we want to be able to remote manage the server
  os_profile_windows_config {
    provision_vm_agent              = true
    
    #Set the winrm protocol
    winrm {
      protocol                      = "HTTP"
    }

    #We need to poke a hole in the firewall on the VM for winrm to work
    #I am not generating a cert here so I want to use HTTP
    ##We should be generating a cert and installing it, but this is a PoC
    additional_unattend_config {
      pass                          = "oobeSystem"
      component                     = "Microsoft-Windows-Shell-Setup"
      setting_name                  = "FirstLogonCommands"
      content                       = file("firstLogonCommands.xml")
    }
  }

  provisioner "local-exec" {
    working_dir                     = "../infrastructure.azure.ansible/"
    #Check if the file exists, if it doesn't create it
    ##Don't want to delete because there might be other settings there we want to keep
    #Update the connection type
    #Update the cert validation <Variablize this>
    #Update the Storage Account Name
    #Update the Storage Account Primary Access Key <Move this to vault>
    #Update the Storage Share Name
    #Set the ansible_winrm_transport <maybe able to get rid of this> <NEEDS TESTING>
    #Set the ansible_port <Once SSL is enabled this should be removed>
    command = <<COMMAND
      test -f "group_vars/tag_app_octopus" || touch group_vars/tag_app_octopus
      sed -i.bak "/ansible_connection/d" group_vars/tag_app_octopus && echo ansible_connection: winrm >> group_vars/tag_app_octopus;
      sed -i.bak "/ansible_winrm_server_cert_validation/d" group_vars/tag_app_octopus && echo ansible_winrm_server_cert_validation: ignore >> group_vars/tag_app_octopus;
      sed -i.bak "/storage_account/d" group_vars/tag_app_octopus && echo storage_account: \'${azurerm_storage_account.sa.name}\' >> group_vars/tag_app_octopus;
      sed -i.bak "/storage_password/d" group_vars/tag_app_octopus && echo storage_password: \'${azurerm_storage_account.sa.primary_access_key}\' >> group_vars/tag_app_octopus;
      sed -i.bak "/share_name/d" group_vars/tag_app_octopus && echo share_name: \'${azurerm_storage_share.fs.name}\' >> group_vars/tag_app_octopus;
      sed -i.bak "/ansible_winrm_transport/d" group_vars/tag_app_octopus && echo ansible_winrm_transport: ntlm >> group_vars/tag_app_octopus;
      sed -i.bak "/ansible_port/d" group_vars/tag_app_octopus && echo ansible_port: 5985 >> group_vars/tag_app_octopus;
    COMMAND
  }

  provisioner "local-exec" {
    when                            = destroy
    working_dir                     = "../infrastructure.azure.ansible/"
    command                         = "rm group_vars/tag_app_octopus"
  }
}

resource "azurerm_network_security_rule" "lb_inbound" {
  resource_group_name         = azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.nsg.name
  depends_on                  = [
    azurerm_network_security_group.nsg
  ]

  name                        = "${var.prefix}-allow-lb-inbound"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  source_address_prefix       = "Internet"
  source_port_range           = "*"
  destination_address_prefix  = azurerm_public_ip.lb_ip.ip_address
  destination_port_ranges     = [ "80", "443" ]
  protocol                    = "TCP"
}

resource "azurerm_network_security_rule" "allow_crosstalk" {
  resource_group_name         = azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.nsg.name
  depends_on                  = [
    azurerm_network_security_group.nsg
  ]
  
  name                        = "${var.prefix}-allow-crosstalk"
  priority                    = 800
  direction                   = "Inbound"
  access                      = "Allow"
  source_address_prefix       = local.iprange
  source_port_range           = "*"
  destination_address_prefix  = local.iprange
  destination_port_range      = "*"
  protocol                    = "*"
}

resource "azurerm_network_security_rule" "name" {
  resource_group_name         = azurerm_resource_group.rg.name
  network_security_group_name = azurerm_network_security_group.nsg.name
  depends_on                  = [
    azurerm_network_security_group.nsg
  ]
  
  name                        = "TEMP_allow_rdp_setup"
  priority                    = 150
  direction                   = "Inbound"
  access                      = "Allow"
  source_address_prefix       = "Internet"
  source_port_range           = "*"
  destination_address_prefix  = "*"
  destination_port_ranges     = [ "3389", "5985", "5986" ]
  protocol                    = "TCP"
}