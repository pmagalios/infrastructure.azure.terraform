variable "region"  {
    description = "The region the resource group will be created in"
}
variable "prefix" {
    description = "The prefix to be appended to all resources"
}